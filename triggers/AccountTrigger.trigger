trigger AccountTrigger on Account (before insert, before update) {
	Executable sequencer = SequencerFactory.createSequencer(Account.sObjectType);
	sequencer.execute();
}