/**
 *	Used to instantiate  concrete Sobject's Sequencer.
 *  @author Maksym Gorinshteyn
 */
public class SequencerFactory {
	/**
	*	Creates concrete Sobject's Sequencer.
	*	@param 	sObjectType specifies Sequencer.
	*	@return Executable concrete Sequencer.
	*/
	public static Executable createSequencer(Schema.SObjectType sObjectType){
		Executable sequencer;
		String soType = String.valueOf(sObjectType).remove('__c');
		Type implementation= Type.forName(soType+'Sequencer');
		if(implementation!=null){
			sequencer = (Executable)implementation.newInstance();
	    }
	    return  sequencer;
    }
}