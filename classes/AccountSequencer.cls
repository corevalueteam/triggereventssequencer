/**
 *  Implementation of Executabe interface for Account.
 *  @author Maksym Gorinshteyn
*/
public class AccountSequencer implements Executable {
	
	public void execute(){
		if(Trigger.isBefore){
			new AccountCheckDuplicates().checkDuplicates(Trigger.new);
		}
	}
}