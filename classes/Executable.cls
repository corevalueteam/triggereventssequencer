/**
 *  Provides executable behavior for descendant classes.
 *  @author Maksym Gorinshteyn
*/
public interface Executable {

    void execute();
}